﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
namespace droplet_devours
{

    public class Screen : DrawableGameComponent
    {
        public SpriteBatch sprBatch;
        public Screen(Game game) : base(game)
        {
            Visible = false;
            Enabled = false;
            sprBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
        }
        public void Show()
        {
            Visible = true;
            Enabled = true;
        }
        public void Hide()
        {
            Visible = false;
            Enabled = false;
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}