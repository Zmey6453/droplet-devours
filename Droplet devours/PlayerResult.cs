﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace droplet_devours
{

    public class PlayerResult
    {
        public string Name { get; private set; }
        public int Mind { get; set; } = -1;
        public int Health { get; set; } = -1;

        public PlayerResult(string name)
        {
            Name = name;
        }

        public PlayerResult(string buffer, bool t)
        {
            string[] words = buffer.Split(new char[] { ' ' });

            Name = words[0];
            Mind = Convert.ToInt32(words[1]);
            Health = Convert.ToInt32(words[2]);
        }

        public string ResultToString()
        {
            return Name + " " + Mind.ToString() + " " + Health.ToString();
        }


    }
}
