﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;using System.IO;
namespace droplet_devours
{
    public class RatingScreen : Screen
    {
        private Texture2D topEmpty, ratingScreen, annotation;
        private List<PlayerResult> rating = new List<PlayerResult>();
        private ResultComparer rc;
        private bool empty = true;
        private SpriteFont font;
        private Vector2 textpos = new Vector2(460,150);
        private int verticaloffset = 0;



        public RatingScreen(World game, ref Texture2D topEmptyspr, ref Texture2D ratingScreen, ref Texture2D annotation, ref SpriteFont font) : base(game)
        {
            topEmpty = topEmptyspr;
            rc = new ResultComparer();
            this.ratingScreen = ratingScreen;
            this.font = font;
            this.annotation = annotation;

            //считать из файла в список резалты, используя их конструктор по строке
            ReadResults();
            rating.Sort(rc);


        }

        private void ReadResults()
        {
            try
            {
                StreamReader f = new StreamReader("Rating.txt");
                string s;
                while (!f.EndOfStream)
                {
                    s = f.ReadLine();
                    rating.Add(new PlayerResult(s, true));
                }
                f.Close();
                empty = false;
            }
            catch
            {
                empty = true; //если файл пуст, то вывод "таблица рейтинга пуста"
            }
            finally
            {
                if (rating.Count == 0) empty = true;
            }
        }


        public override void Draw(GameTime gameTime)
        {
           

            sprBatch.Draw(ratingScreen, new Vector2(415, 30), Color.White);
            
            if (empty) sprBatch.Draw(topEmpty, new Vector2(415, 150), Color.White); // топ пуст
            else
            { //не пуст, вывод списка результатов
                foreach (PlayerResult result in rating)
                {
                    sprBatch.DrawString(font, result.Name, new Vector2(textpos.X, textpos.Y + verticaloffset), Color.CadetBlue);
                    sprBatch.DrawString(font, result.Mind.ToString(), new Vector2(textpos.X + 450, textpos.Y + verticaloffset), Color.CadetBlue);
                    sprBatch.DrawString(font, result.Health.ToString(), new Vector2(textpos.X + 600, textpos.Y + verticaloffset), Color.CadetBlue);
                    verticaloffset += 50;
                }
                verticaloffset = 0;
               
               
                sprBatch.Draw(annotation, new Vector2(415, 940), Color.White); 
            }
            base.Draw(gameTime);
        }

        public bool ValidateName(string name)
        {
            foreach (PlayerResult result in rating)
            {
                if (result.Name == name) return false;
            }
            return true;
        }

        public void AddResult(PlayerResult result)
        {
            rating.Add(result);
            rating.Sort(rc);
            WriteResults();

        }

        private void WriteResults()
        {
            StreamWriter f = new StreamWriter("Rating.txt", false);
            foreach (PlayerResult result in rating)
            {
                f.WriteLine(result.ResultToString());
            }
            f.Close();
        }

    }
}