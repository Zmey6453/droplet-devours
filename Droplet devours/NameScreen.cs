﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Microsoft.Xna.Framework.Content;

namespace droplet_devours
{
    public class NameScreen : Screen
    {
         
        Texture2D nameExistsspr, enterName;
        private KeyboardState ks;
        public string Name { get; private set; } = String.Empty;
        private int singboardLength = 10;
        private List<Texture2D> nameLetters = new List<Texture2D>();
        private const int aNum = 26;
        private Texture2D[] alphabet = new Texture2D[aNum];
        private int c = 0;
        private int skip = 0;

        public bool nameReady = false;
        private bool nameExists = false;
        RatingScreen ratingScreen;
        private int wh = 60;
        private Vector2 singboardPosition = new Vector2(1747,440); //1747 и левее, 440 и ниже

        public NameScreen(World game,  ref Texture2D nameExistsspr, ref Texture2D enterName, ref RatingScreen ratingScreen) : base(game)
        {

            this.nameExistsspr = nameExistsspr;
           
            this.ratingScreen = ratingScreen;
            this.enterName = enterName;

        }

        public void LoadLetters(ref Texture2D letterTex) {
            alphabet[c++] = letterTex;
        }

        public override void Update(GameTime gameTime) {
            skip++;
            if (skip == 5) {
                EnterLetter();
                skip = 0;
            }
     


        }


        private void EnterLetter()
        {
            ks = Keyboard.GetState();
            if (Name.Length < singboardLength)
            {
                if (ks.IsKeyDown(Keys.A))
                {
                    //вывод спрайта на экран (а точнее сдвиг позиции первого на 60 влево и занесение текстуры в конец очереди букв на отрисовку)

                    Name += "A";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[0]);

                }
                if (ks.IsKeyDown(Keys.B))
                {
                    Name += "B";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[1]);
                }
                if (ks.IsKeyDown(Keys.C))
                {
                    Name += "C";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[2]);
                }
                if (ks.IsKeyDown(Keys.D))
                {
                    Name += "D";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[3]);
                }
                if (ks.IsKeyDown(Keys.E))
                {
                    Name += "E";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[4]);
                }
                if (ks.IsKeyDown(Keys.F))
                {
                    Name += "F";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[5]);
                }
                if (ks.IsKeyDown(Keys.G))
                {
                    Name += "G";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[6]);
                }
                if (ks.IsKeyDown(Keys.H))
                {
                    Name += "H";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[7]);
                }
                if (ks.IsKeyDown(Keys.I))
                {
                    Name += "I";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[8]);
                }
                if (ks.IsKeyDown(Keys.J))
                {
                    Name += "J";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[9]);
                }
                if (ks.IsKeyDown(Keys.K))
                {
                    Name += "K";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[10]);
                }
                if (ks.IsKeyDown(Keys.L))
                {
                    Name += "L";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[11]);
                }
                if (ks.IsKeyDown(Keys.M))
                {
                    Name += "M";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[12]);
                }
                if (ks.IsKeyDown(Keys.N))
                {
                    Name += "N";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[13]);
                }
                if (ks.IsKeyDown(Keys.O))
                {
                    Name += "O";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[14]);
                }
                if (ks.IsKeyDown(Keys.P))
                {
                    Name += "P";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[15]);
                }
                if (ks.IsKeyDown(Keys.Q))
                {
                    Name += "Q";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[16]);
                }
                if (ks.IsKeyDown(Keys.R))
                {
                    Name += "R";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[17]);
                }
                if (ks.IsKeyDown(Keys.S))
                {
                    Name += "S";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[18]);
                }
                if (ks.IsKeyDown(Keys.T))
                {
                    Name += "T";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[19]);
                }
                if (ks.IsKeyDown(Keys.U))
                {
                    Name += "U";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[20]);
                }
                if (ks.IsKeyDown(Keys.V))
                {
                    Name += "V";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[21]);
                }
                if (ks.IsKeyDown(Keys.W))
                {
                    Name += "W";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[22]);
                }
                if (ks.IsKeyDown(Keys.X))
                {
                    Name += "X";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[23]);
                }
                if (ks.IsKeyDown(Keys.Y))
                {
                    Name += "Y";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[24]);
                }
                if (ks.IsKeyDown(Keys.Z))
                {
                    Name += "Z";
                    singboardPosition.X -= wh;
                    nameLetters.Add(alphabet[25]);
                }
            }
            //если строка не пустая и нажался ентер, то передача имени для проверки валидности в ratingScreen
         
            if (!string.IsNullOrEmpty(Name) && ks.IsKeyDown(Keys.Enter))
            {
                // if (RatingScreen.ValidateName(Name)) nameReady = true;
                if (ratingScreen.ValidateName(Name)) nameReady = true;
                else
                {
                    nameReady = false;
                    nameExists = true;

                }

            }



            if (ks.IsKeyDown(Keys.Back))
            { //последний символ из строки удалить, позишн начала вывески сдвинуть на 60 вправо, последнюю текстуру из очереди удалить
                if (!string.IsNullOrEmpty(Name))
                {
                    Name = Name.Remove(Name.Length - 1);
                    nameLetters.RemoveAt(nameLetters.Count - 1);
                    singboardPosition.X += wh;
                    nameExists = false;
                }
            }
        }

        public void ClearName()
        {
            Name = string.Empty;
            singboardPosition = new Vector2(1747, 440);
            nameLetters.Clear();
            nameReady = false;
        }


        public override void Draw(GameTime gameTime)
        {
            
                int offset = 0;
                // Vector2 letterpos = new Vector2(singboardPosition.Y) 
                foreach (Texture2D tex in nameLetters)
                {
                    sprBatch.Draw(tex, new Vector2(singboardPosition.X + offset, singboardPosition.Y), Color.White);
                    offset += wh;
                }
                if (nameExists) sprBatch.Draw(nameExistsspr, new Vector2(0, 930), Color.White);
                else if (this.Enabled) sprBatch.Draw(enterName, new Vector2(0, 930), Color.White);
            //sprBatch.Draw(test, new Vector2(600, 600), Color.White);


            //Выводим изображения
            //  sprBatch.Draw(backTexture, backRectangle, Color.White);


            base.Draw(gameTime);
        }
    }
}
