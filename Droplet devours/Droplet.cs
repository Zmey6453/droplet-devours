﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;


namespace droplet_devours
{
    public partial class Droplet : DrawableGameComponent
    {
        enum State {
            sleep,
            pursuitUP,
            pursuitDOWN,
            pursuitLEFT,
            pursuitRIGHT,
            pursuitUPRIGHT,
            pursuitDOWNRIGHT,
            pursuitDOWNLEFT,
            pursuitUPLEFT
        }
        private State state;
        private Texture2D texture;
        private Rectangle rectangle;
        private Vector2 position;
        private Rectangle screen;
        private int normalspeed = 15;
        private int pursuitspeed = 7;
        private int initscale = 500;
        public int mymind { get; private set; }
        public int myhealth { get; private set; }
        private Texture2D scale;
        private Vector2 scaleposM;
        private Rectangle scalerecM;
        private Vector2 scaleposH;
        private Rectangle scalerecH;
        private int scalesizeM;
        private int scalesizeH;
        private int gridstep;
        World mygame;
        private int flairradius = 4;
        KeyboardState kbState;

        private const int frames = 3;
        private const int framesPerSec = 3;
        private int numberOfFrame = 0;
        private float timePerFrame;
        private float totalElapsed;
        private int widthFrame;


        public Droplet(World game, ref Texture2D scale, ref Texture2D texture, Rectangle rectangle, Vector2 position, int gridstep) : base(game)
        {
            state = State.sleep;
            mymind = initscale;
            myhealth = initscale;
            this.scale = scale;
            this.texture = texture;
            this.rectangle = rectangle;
            this.position = position;
            screen = new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height);
            scalesizeM = initscale;
            scalesizeH = initscale;
            scaleposM = new Vector2(40, 10);
            scalerecM = new Rectangle(0, 0, scalesizeM, 40);
            scaleposH = new Vector2(40, 60);
            scalerecH = new Rectangle(0, 0, scalesizeH, 40);
            this.gridstep = gridstep;
            widthFrame = gridstep;
            mygame = game;
            timePerFrame = (float)1 / framesPerSec;
            // spriteongridX = new List<World.Coord>((int)(rectangle.Width / gridstep));// ~сколько ячеек в ширину
            // spriteongridY = new List<World.Coord>((int)(rectangle.Height/ gridstep));// ~сколько занимает ячеек в высоту
            // spriteongridX = (int)(rectangle.Width / gridstep);
            //spriteongridY = (int)(rectangle.Height / gridstep);
        }
     
        public override void Initialize()
        {
             base.Initialize();
        }

        private void FindPray()
        {
            World.Coord obj = new World.Coord(-1, -1, -1);
            int i = 0, j = 0;
            int x = (int)(position.X / gridstep);
            int y = (int)(position.Y / gridstep);
            

            for (int rad = 1; rad <= flairradius; rad++) // приоритет целей, первым проверяются ячейки вокруг капли, потом радиус проверки увеличивается 
            {
                int borderright = x + rad + 1;
                int borderbottom = y + rad + 1;

                for (i = y - rad; i < borderbottom; i++) // строки, координаты сеточные
                {
                    for (j = x - rad; j < borderright; j++) //столбцы
                    {
                        obj = mygame.WhatIsIt(i, j);
                        if (obj.type == 1) //еда нашлась
                        {
                            x -= j;
                            y -= i;
                            if (x == 0)
                            {
                                //if (y == 0)
                                //{
                                //    state = State.sleep;
                                //    return;
                                //}
                                if (y > 0)
                                {
                                    state = State.pursuitUP;
                                    return;
                                }
                                if (y < 0)
                                {
                                    state = State.pursuitDOWN;
                                    return;
                                }
                            }
                            if (x > 0)
                            {
                                if (y == 0)
                                {
                                    state = State.pursuitLEFT;
                                    return;
                                }
                                if (y > 0)
                                {
                                    state = State.pursuitUPLEFT;
                                    return;
                                }
                                if (y < 0)
                                {
                                    state = State.pursuitDOWNLEFT;
                                    return;
                                }

                            }
                            if (x < 0)
                            {
                                if (y == 0)
                                {
                                    state = State.pursuitRIGHT;
                                    return;
                                }
                                if (y > 0)
                                {
                                    state = State.pursuitUPRIGHT;
                                    return;
                                }
                                if (y < 0)
                                {
                                    state = State.pursuitDOWNRIGHT;
                                    return;
                                }

                            }

                        }
                        else state = State.sleep;
                    }
                }
            }


        }

        void ChangeFrame(float elpT)
        {           
            totalElapsed += elpT;           
            if (totalElapsed > timePerFrame)
            {
                if (numberOfFrame == frames - 1)
                {
                    numberOfFrame = 0;
                }
                else numberOfFrame++;              
                rectangle = new Rectangle((int)widthFrame * numberOfFrame, 0, widthFrame, widthFrame);
                totalElapsed = 0;
            }

        }

        private void GoUp(int speed)
        {
            position.Y -= speed;
            if (position.Y <= screen.Top)
            {
                position.Y = screen.Top;
            }
            else
            {
                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.Y = obj.row * gridstep + gridstep;
                }
                if (obj.type == 3 && kbState.IsKeyDown(Keys.O) == true)
                {
                    int st = mygame.GetState();
                    mygame.SetState(++st);
                    position.Y = screen.Height - rectangle.Height;
                }

            }
        }
        private void GoDown(int speed)
        {
            position.Y += speed;
            if (position.Y >= screen.Height - rectangle.Height)
            {
                position.Y = screen.Height - rectangle.Height;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y + rectangle.Height));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.Y = obj.row * gridstep - rectangle.Height;
                }
                if (obj.type == 3 && kbState.IsKeyDown(Keys.O)==true)
                {
                    int st = mygame.GetState();
                    mygame.SetState(++st);
                    position.Y = screen.Height - rectangle.Height;
                }

            }
        }
        private void GoLeft(int speed)
        {
            position.X -= speed;
            if (position.X <= screen.Left)
            {
                position.X = screen.Left;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.X = obj.column * gridstep + gridstep;
                }
                if (obj.type == 3 && kbState.IsKeyDown(Keys.O) == true)
                {
                    int st = mygame.GetState();
                    mygame.SetState(++st);
                    position.Y = screen.Height - rectangle.Height;
                }

            }
        }
        private void GoRight(int speed)
        {
            position.X += speed;
            if (position.X >= screen.Width - rectangle.Width)
            {
                position.X = screen.Width - rectangle.Width;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X + rectangle.Width, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.X = obj.column * gridstep - rectangle.Width;
                }
                if (obj.type == 3 && kbState.IsKeyDown(Keys.O) == true)
                {
                    int st = mygame.GetState();
                    mygame.SetState(++st);
                    position.Y = screen.Height - rectangle.Height;
                }

            }
        }

        public override void Update(GameTime gameTime)
        {
            kbState = Keyboard.GetState();
            ChangeFrame((float)gameTime.ElapsedGameTime.TotalSeconds);
            FindPray();
            switch (state)
            {
                case State.pursuitDOWN:
                    {
                        GoDown(pursuitspeed);
                        break;
                    }
                case State.pursuitDOWNLEFT:
                    {
                        GoDown(pursuitspeed);
                        GoLeft(pursuitspeed);
                        break;
                    }
                case State.pursuitLEFT:
                    {                    
                        GoLeft(pursuitspeed);
                        break;
                    }
                case State.pursuitUPLEFT:
                    {
                        GoUp(pursuitspeed);
                        GoLeft(pursuitspeed);
                        break;
                    }
                case State.pursuitUP:
                    {
                        GoUp(pursuitspeed);
                        break;
                    }
                case State.pursuitUPRIGHT:
                    {
                        GoUp(pursuitspeed);
                        GoRight(pursuitspeed);
                        break;
                    }
                case State.pursuitRIGHT:
                    {
                        GoRight(pursuitspeed);
                        break;
                    }
                case State.pursuitDOWNRIGHT:
                    {
                        GoDown(pursuitspeed);
                        GoRight(pursuitspeed);
                        break;
                    }
                case State.sleep:
                    {
                        break;
                    }
            }




            if (kbState.IsKeyDown(Keys.Up))
            {
                GoUp(normalspeed);
            }

            if (kbState.IsKeyDown(Keys.Down))
            {
                GoDown(normalspeed);
            }

            if (kbState.IsKeyDown(Keys.Left))
            {
                GoLeft(normalspeed);
            }

            if (kbState.IsKeyDown(Keys.Right))
            {
                GoRight(normalspeed);
            }



    

            base.Update(gameTime);
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        public Rectangle GetRectangle()
        {
            return rectangle;
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sprBatch =  (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            sprBatch.Draw(texture, position, rectangle, Color.White);
            //  sprBatch.Draw(sprTexture, sprPosition, sprRectangle, Color.White, 0.0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 3);
            sprBatch.Draw(scale, scaleposM, new Rectangle(0, 0, scalesizeM, 40), Color.White);
            sprBatch.Draw(scale, scaleposH, new Rectangle(0, 0, scalesizeH, 40), Color.White);
            base.Draw(gameTime);
        }

        public void Devouring(int mind, int health)
        {
            //state = State.sleep;
            mymind += mind;
            scalesizeM += mind;
           
            myhealth += health;
            scalesizeH += health;
 
            if (mymind > initscale)
            {
                mymind = initscale;
                scalesizeM = initscale;
            }
            if (myhealth > initscale)
            {
                myhealth = initscale;
                scalesizeH = initscale;
            }
            if (mymind <= 0)
            {
                mymind = 0;
                mygame.SetState(0);
            }
            if (myhealth <= 0)
            {                
                myhealth = 0;
                mygame.SetState(0);
            }
        }

        //public int GetMind()
        //{
        //    return mymind;

        //}

        //public int GetHealth()
        //{
        //    return myhealth;

        //}
    }
}

    

