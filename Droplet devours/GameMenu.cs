﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace droplet_devours
{
    public partial class GameMenu : Screen
    {
        private World mygame;
        private Texture2D menu, start, rating;
        private Rectangle rec = new Rectangle(0, 0, 374, 120);
        private MouseState mState;
        private RatingScreen ratingScreen; 
        private NameScreen nameScreen; 
        private Vector2 menupos = new Vector2(0, 130);
        private Vector2 startpos = new Vector2(0, 250);
        private Vector2 ratingpos = new Vector2(0, 370);

        public GameMenu(World game, ref Texture2D menu, ref Texture2D start, ref Texture2D rating, ref RatingScreen ratingScreen, ref NameScreen nameScreen) : base(game)
        {
            this.menu = menu;
            this.start = start;
            this.rating = rating;
           
            this.ratingScreen = ratingScreen;
            this.nameScreen = nameScreen;
            InitializeComponent();
        }


        public override void Update(GameTime gameTime)
        {
            mState = Mouse.GetState();
            if (mState.LeftButton == ButtonState.Pressed)
            {
                if (isHit(startpos, rec, new Vector2(mState.X, mState.Y)))
                {
                    ratingScreen.Hide();
                    nameScreen.Show();
                }
                if (isHit(ratingpos, rec, new Vector2(mState.X, mState.Y)))
                {
                    //nameScreen.Hide();
                    nameScreen.Enabled = false;
                    ratingScreen.Show();
                }
            }
        }

        private bool isHit(Vector2 p, Rectangle r, Vector2 point)
        {
            if (point.X >= p.X && point.X <= p.X + r.Width && point.Y >= p.Y && point.Y <= p.Y + r.Height) return true;
            else return false;
        }

        public override void Draw(GameTime gameTime)
        {
            //sprBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            sprBatch.Draw(menu, menupos, rec, Color.White);
            sprBatch.Draw(start, startpos, rec, Color.White);
            sprBatch.Draw(rating, ratingpos, rec, Color.White);

            base.Draw(gameTime);
        }

    }
}

