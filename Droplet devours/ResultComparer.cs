﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace droplet_devours
{
    class ResultComparer : IComparer<PlayerResult>
    {

        public int Compare(PlayerResult first, PlayerResult second)
        {

            if (first.Mind > second.Mind)
            {
                return -1;
            }
            else if (first.Mind < second.Mind)
            {
                return 1;
            }
            else if (first.Mind == second.Mind)
            {
                if (first.Health > second.Health)
                {
                    return -1;
                }
                else if (first.Health < second.Health)
                {
                    return 1;
                }
                else if (first.Health == second.Health)
                {
                    int result = String.Compare(first.Name, second.Name);
                    if (result < 0)
                    {
                        return -1;
                    }
                    else if (result > 0)
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }
    }
}
