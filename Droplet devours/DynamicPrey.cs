﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;

namespace droplet_devours
{
    class DynamicPrey:Prey
    {
        private const int frames = 6;
        private const int framesPerSec = 6;
        private int numberOfFrame = 0;
        private float timePerFrame;
        private float totalElapsed;
        private int widthFrame;
        private Rectangle screen;
        private int speed = 6;
        // private Vector2 drpos;
        //private Rectangle drrec;
        private Texture2D pursuitTextureL;
        private Texture2D pursuitTextureR;
        private Texture2D peaceTexture;
        private int flairradius = 6;

        public DynamicPrey(World game, ref Droplet droplet, int minddamage, int healthdamage, int gridstep,  ref Texture2D peaceTexture, 
            ref Texture2D pursuitTextureR, ref Texture2D pursuitTextureL, Rectangle rectangle, Vector2 position) : base(game, ref droplet, minddamage, 
                healthdamage, gridstep, ref peaceTexture, rectangle, position)
        {
            this.pursuitTextureL = pursuitTextureL;
            this.pursuitTextureR = pursuitTextureR;
            this.peaceTexture = peaceTexture;
            widthFrame = gridstep;
            timePerFrame = (float)1 / framesPerSec;
            screen = new Rectangle(0, 0, game.Window.ClientBounds.Width, game.Window.ClientBounds.Height);
            currenttexture = peaceTexture;

        }
        public override void Initialize()
        {
            base.Initialize();
        }
        void ChangeFrame(float elpT)
        {
            totalElapsed += elpT;
            if (totalElapsed > timePerFrame)
            {
                if (numberOfFrame == frames - 1)
                {
                    numberOfFrame = 0;
                }
                else numberOfFrame++;
                rectangle = new Rectangle((int)widthFrame * numberOfFrame, 0, widthFrame, widthFrame);
                totalElapsed = 0;
            }

        }
        private void GoUp()
        {
            position.Y -= speed;
            if (position.Y <= screen.Top)
            {
                position.Y = screen.Top;
            }
            else
            {
                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.Y = obj.row * gridstep + gridstep;
                }

            }
        }
        private void GoDown()
        {
            position.Y += speed;
            if (position.Y >= screen.Height - rectangle.Height)
            {
                position.Y = screen.Height - rectangle.Height;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y + rectangle.Height));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.Y = obj.row * gridstep - rectangle.Height;
                }

            }
        }
        private void GoLeft()
        {
            position.X -= speed;
            if (position.X <= screen.Left)
            {
                position.X = screen.Left;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.X = obj.column * gridstep + gridstep;
                }

            }
        }
        private void GoRight()
        {
            position.X += speed;
            if (position.X >= screen.Width - rectangle.Width)
            {
                position.X = screen.Width - rectangle.Width;
            }
            else
            {

                World.Coord obj = mygame.WhatIsIt(new Vector2(position.X + rectangle.Width, position.Y));
                if (obj.type == 2 || obj.type == 3)
                {
                    position.X = obj.column * gridstep - rectangle.Width;
                }

            }
        }

        private bool FindDroplet()
        {
            drpos = droplet.GetPosition();
            drrec = droplet.GetRectangle();
            Rectangle radius = new Rectangle((int)position.X - flairradius * gridstep, (int)position.Y - flairradius * gridstep, (flairradius * 2 + 1) * gridstep, (flairradius * 2 + 1) * gridstep);

            if (drpos.X < radius.X + radius.Width && //капля попала в поле зрения
            drpos.X + drrec.Width > radius.X && 
            drpos.Y < radius.Y + radius.Height &&
            drpos.Y + drrec.Height > radius.Y)
            {
                return true;
            }
            else return false;

        }

        private void Move()
        {

            if (position.X - (drpos.X + drrec.Width / 2) > 0)            
            {
                currenttexture = pursuitTextureL;
                GoLeft();

            }
            if ((position.X + drrec.Width / 2) - drpos.X < 0)
            {
                currenttexture = pursuitTextureR;
                GoRight();
            }
            if (position.Y - (drpos.Y + drrec.Height / 2) > 0)
            {
                GoUp();
            }
            if ((position.Y + drrec.Height / 2) - drpos.Y < 0)
            {
                GoDown();
            }
        }

        public override void Update(GameTime gameTime)
        {
            ChangeFrame((float)gameTime.ElapsedGameTime.TotalSeconds);
            if (FindDroplet())
            {
                Move();
            }
            else
            {
                currenttexture = peaceTexture;
            }


            if (base.IsCollide())
            {
                droplet.Devouring(minddamage, healthdamage);
                mygame.SetObjOnLevel(0, (int)(position.Y / gridstep), (int)(position.X / gridstep));
                mygame.Components.Remove(this);
            }

        }

        
    }
}
