﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;


namespace droplet_devours
{
    public partial class Prey : DrawableGameComponent
    {
        protected Texture2D currenttexture;
        protected Rectangle rectangle;
        protected Vector2 position;
        protected Droplet droplet;
        protected int minddamage;
        protected int healthdamage;
        protected World mygame;
        protected int gridstep;
        protected Vector2 drpos;
        protected Rectangle drrec;

        public Prey(World game, ref Droplet droplet, int minddamage, int healthdamage, int gridstep, ref Texture2D texture, Rectangle rectangle, Vector2 position) : base(game)
        {
            mygame = game;
            this.droplet = droplet;
            this.minddamage = minddamage;
            this.healthdamage = healthdamage;
            this.currenttexture = texture;
            this.rectangle = rectangle;
            this.position = position;
            this.gridstep = gridstep;
        

        }
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (IsCollide())
            {
                droplet.Devouring(minddamage, healthdamage);
                mygame.SetObjOnLevel(0, (int)(position.Y / gridstep), (int)(position.X / gridstep));
                mygame.Components.Remove(this);
            }

        }

        public bool IsCollide()
        {
            drpos = droplet.GetPosition();
            drrec = droplet.GetRectangle();
            if (drpos.X < position.X + rectangle.Width &&
            drpos.X + drrec.Width > position.X &&
            drpos.Y < position.Y + rectangle.Height &&
            drpos.Y + drrec.Height > position.Y)
            {
                return true;
            }
            else return false;
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sprBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            sprBatch.Draw(currenttexture, position, rectangle, Color.White);
           // sprBatch.Draw(currenttexture, position, rectangle, null, null, 0f, null, Color.White, SpriteEffects.None, 0.0f);
            // sprBatch.Draw(sprTexture, sprPosition, sprRectangle,Color.White, 0.0f, Vector2.Zero, Vector2.One ,SpriteEffects.None, 2.0f);

            base.Draw(gameTime);
        }

        public void Destroy()
        {
            mygame.Components.Remove(this);
        }
    }
}
