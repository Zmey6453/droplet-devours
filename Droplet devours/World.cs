﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace droplet_devours
{
  
    public class World : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Texture2D Fon;
        private Texture2D fgo, fm,f1,f2,f3;
        private Texture2D dr;
        private Texture2D burgerspr, stonespr, plumspr, grassspr, mushroomspr,bluegrassspr,pumpkinspr, agaricspr, polyporespr,peasspr;
        private Texture2D decor2;
        private Texture2D grid;
        private Texture2D centipedespr,centipedePRspr, centipedePLspr;
        Droplet droplet;
        private Texture2D sprscales,sprscale ;
        private Texture2D menuspr, start1spr, rating1spr;
        private Texture2D nameExistsspr, enterNamespr;
        private Texture2D topEmptyspr, ratingScreenspr, annotationspr;
        PlayerResult result;

        StaticPrey burger, stone,plum,grass,mushroom, bluegrass,pumpkin,agaric,polypore,peas;
        DynamicPrey centipede, centipede2;
        RatingScreen ratingScreen;
        NameScreen nameScreen;
        GameMenu gameMenu;
        Texture2D testRating, testName;
        SpriteFont font;

        private const int aNum = 26;
        private Texture2D[] alphabetspr = new Texture2D[aNum];
        private int c = 0;

        private Texture2D gameoverspr, winspr;
        private Rectangle gameoverrec;
        private Vector2 gameoverpos;
        bool f = true;
        private const int frames = 4;
        private const int framesPerSec = 4;
        private int numberOfFrame = 0;
        private float timePerFrame;
        private float totalElapsed;
        private int widthFrame = 819;
        private int heightFrame = 255;

        private int[,] level;
        //List<string[]> rating = new List<string[]>();

        private int gridstep = 60;

        enum State
        {
            gameover,
            menu,
            level1,
            level2,
            win
        }
        private State state;

        public struct Coord {
            public int type;
            public int row;
            public int column;

            public Coord(int type, int row, int column) {
                this.type = type;
                this.row = row;
                this.column = column;
            }
        }
       

        public World()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }


        protected override void Initialize()
        {
            
            graphics.PreferredBackBufferWidth = 1920;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            timePerFrame = (float)1 / framesPerSec;



            base.Initialize();
        }
        public void SetObjOnLevel(int type, int i, int j)
        {

            level[i, j] = type;
        }


   
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), spriteBatch);

           // fgo = Content.Load<Texture2D>("fongameover");
            fm = Content.Load<Texture2D>("fonmenu");
            f1 = Content.Load<Texture2D>("fonlvl1");
            f2 = Content.Load<Texture2D>("fonlvl2");
           // f3 = Content.Load<Texture2D>("fonlvl3");
            dr = Content.Load<Texture2D>("droplet");

            burgerspr = Content.Load<Texture2D>("burger");
            stonespr = Content.Load<Texture2D>("8");
            plumspr = Content.Load<Texture2D>("7");
            grassspr = Content.Load<Texture2D>("39");
            agaricspr = Content.Load<Texture2D>("20");
            peasspr = Content.Load<Texture2D>("17");
            mushroomspr = Content.Load<Texture2D>("44");
            pumpkinspr = Content.Load<Texture2D>("9");
            bluegrassspr = Content.Load<Texture2D>("47");
            polyporespr = Content.Load<Texture2D>("23");

            sprscales = Content.Load<Texture2D>("scales");
            sprscale = Content.Load<Texture2D>("scale");
            decor2 = Content.Load<Texture2D>("table");
            grid = Content.Load<Texture2D>("grid");
            centipedespr = Content.Load<Texture2D>("centipede");
            centipedePLspr = Content.Load<Texture2D>("centipedePL");
            centipedePRspr = Content.Load<Texture2D>("centipedePR");
            gameoverspr = Content.Load<Texture2D>("gameover");

            menuspr = Content.Load<Texture2D>("menu");
            start1spr = Content.Load<Texture2D>("start1");
            rating1spr = Content.Load<Texture2D>("rating1");
           // testRating = Content.Load<Texture2D>("testRating");
           // testName = Content.Load<Texture2D>("testName");
            nameExistsspr = Content.Load<Texture2D>("nameExists");
            enterNamespr = Content.Load<Texture2D>("enterName");
            ratingScreenspr = Content.Load<Texture2D>("ratingScreen");
            topEmptyspr = Content.Load<Texture2D>("topEmpty");
            font = Content.Load<SpriteFont>("d_Kallisto");
            winspr = Content.Load<Texture2D>("win");
            annotationspr = Content.Load<Texture2D>("annotation");

            alphabetspr[0] = Content.Load<Texture2D>("a");
            alphabetspr[1] = Content.Load<Texture2D>("b");
            alphabetspr[2] = Content.Load<Texture2D>("c");
            alphabetspr[3] = Content.Load<Texture2D>("d");
            alphabetspr[4] = Content.Load<Texture2D>("e");
            alphabetspr[5] = Content.Load<Texture2D>("f");
            alphabetspr[6] = Content.Load<Texture2D>("g");
            alphabetspr[7] = Content.Load<Texture2D>("h");
            alphabetspr[8] = Content.Load<Texture2D>("i");
            alphabetspr[9] = Content.Load<Texture2D>("j");
            alphabetspr[10] = Content.Load<Texture2D>("k");
            alphabetspr[11] = Content.Load<Texture2D>("l");
            alphabetspr[12] = Content.Load<Texture2D>("m");
            alphabetspr[13] = Content.Load<Texture2D>("n");
            alphabetspr[14] = Content.Load<Texture2D>("o");
            alphabetspr[15] = Content.Load<Texture2D>("p");
            alphabetspr[16] = Content.Load<Texture2D>("q");
            alphabetspr[17] = Content.Load<Texture2D>("r");
            alphabetspr[18] = Content.Load<Texture2D>("s");
            alphabetspr[19] = Content.Load<Texture2D>("t");
            alphabetspr[20] = Content.Load<Texture2D>("u");
            alphabetspr[21] = Content.Load<Texture2D>("v");
            alphabetspr[22] = Content.Load<Texture2D>("w");
            alphabetspr[23] = Content.Load<Texture2D>("x");
            alphabetspr[24] = Content.Load<Texture2D>("y");
            alphabetspr[25] = Content.Load<Texture2D>("z");

            SetState(1);

        }
       
       
        public void SetState(int codeState)
        {
            if (codeState == 0)
            {
                state = State.gameover;
                //  Fon = fgo;
                gameoverpos = new Vector2(1920 / 2 - widthFrame / 2, 1080 / 2 - heightFrame / 2);
                gameoverrec = new Rectangle(0, 0, widthFrame, heightFrame);
                
                result.Mind = droplet.mymind;
                result.Health = droplet.myhealth;
                ratingScreen.AddResult(result);
                Components.Clear();
            }

            if (codeState == 1)
            {
                state = State.menu;
                //   Fon = fm;
                Components.Clear();
                // GameMenu menu = new GameMenu();
                IsMouseVisible = true;

                ratingScreen = new RatingScreen(this, ref topEmptyspr, ref ratingScreenspr, ref annotationspr, ref font);
                nameScreen = new NameScreen(this, ref nameExistsspr, ref enterNamespr, ref ratingScreen);
                for (int i = 0; i < aNum; i++)
                {
                    nameScreen.LoadLetters(ref alphabetspr[i]);
                }
                //nameScreen.Visible = true;

                gameMenu = new GameMenu(this, ref menuspr, ref start1spr, ref rating1spr, ref ratingScreen, ref nameScreen);
                // создание скрытого экрана рейтинга
                // создание скрытого экрана ввода имени
                // добавление в компоненты?
                Components.Add(gameMenu);
                Components.Add(nameScreen);
                Components.Add(ratingScreen);
                gameMenu.Show();
            }
            if (codeState == 2)
            {
                state = State.level1;
                IsMouseVisible = false;
                Components.Clear();
                // Fon = f1;
                level = new int[18, 32]
               {{ 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,2,2,2,2,2,2,2,    2,2},
                { 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,2,2,2,2,2,2,2,    2,2},
                { 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,2,2,2,2,2,2,2,    2,2},
                { 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,3,3,2,2,2,2,2,    2,2},
                { 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,3,3,2,2,2,2,2,    2,2},

                { 2,2,2,2,2,2,2,2,2,2,   2,2,2,2,2,2,2,2,2,2,    2,2,2,3,3,2,2,2,2,2,    2,2},
                { 2,2,2,2,2,2,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,2,2,2,2,    2,2},
                { 2,2,2,2,2,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,2,2,    2,2},
                { 2,2,2,2,0,0,0,0,0,0,   0,0,0,1,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,2,2,    2,2},
                { 2,2,2,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,2,2,    2,2},

                { 2,2,2,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,2,2,    2,2},
                { 2,2,2,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,1,0,0,0,0,0,0,2,2,    2,2},
                { 2,2,2,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,2,2,    2,2},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,2,2,2,    2,2},
                { 0,0,0,0,0,0,0,0,0,0,   1,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,2,2,2,2,    2,2},

                { 0,0,0,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,1,0,2,2,2,2,    2,2},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    1,0},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0}};

                CreateNewObject();
                CreateNewPreys();
            }
            if (codeState == 3)
            {
                state = State.level2;
                //  Fon = f2;
                for (int i = 1; i < Components.Count;)
                    Components.RemoveAt(i);
                level = new int[18, 32]
               {{ 2,2,2,2,2,2,2,2,2,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 2,2,2,2,2,2,2,2,2,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,0,0,0,0,0,0,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},

                { 0,1,0,0,0,0,0,0,0,0,   0,0,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,0,0,0,0,0,0,2,   2,2,2,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 2,2,2,2,2,0,0,0,0,2,   2,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 2,2,2,2,2,0,0,0,2,2,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,2,2,0,0,0,2,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},

                { 0,0,0,2,2,0,0,0,2,2,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0},
                { 0,0,0,2,2,2,0,0,0,2,   2,2,2,2,2,2,2,2,2,2,    0,0,0,0,0,0,0,0,0,2,    2,2},
                { 0,0,0,0,2,2,2,0,0,3,   3,2,2,2,0,0,0,0,0,2,    2,2,0,0,0,2,2,2,2,2,    2,0},
                { 0,0,0,0,0,2,2,0,0,3,   0,0,0,0,0,0,0,0,0,1,    2,2,0,0,2,2,0,0,0,0,    0,0},
                { 0,0,0,0,0,2,2,0,0,2,   0,0,0,0,0,0,0,0,0,0,    0,2,2,2,2,0,0,0,0,0,    0,0},

                { 0,0,0,0,0,2,2,0,0,2,   0,0,0,0,0,0,0,0,0,0,    0,0,0,2,2,0,0,0,0,0,    1,0},
                { 0,0,0,0,0,2,2,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,1,    0,0},
                { 0,0,0,0,0,2,0,0,0,0,   0,0,0,0,0,0,0,0,0,0,    0,0,0,0,0,0,0,0,0,0,    0,0}};

                CreateNewPreys();
            }
            if (codeState == 4)
            {
                state = State.win;
                
                result.Mind = droplet.mymind;
                result.Health = droplet.myhealth;
                ratingScreen.AddResult(result);
                Components.Clear();
            }

        }

        public int GetState()
        {
            return (int)state;
        }


        protected void CreateNewPreys()
        {
            if (state == State.level1)
            {
                burger = new StaticPrey(this, ref droplet, -50, 10, gridstep, ref burgerspr, new Rectangle(0, 0, 60, 60), new Vector2(1260, 660));
                Components.Add(burger);
                stone = new StaticPrey(this, ref droplet, -50, -50, gridstep, ref stonespr, new Rectangle(0, 0, 60, 60), new Vector2(780, 480));
                Components.Add(stone);
                plum = new StaticPrey(this, ref droplet, -50, 10, gridstep, ref plumspr, new Rectangle(0, 0, 60, 60), new Vector2(600, 840));
                Components.Add(plum);
                grass = new StaticPrey(this, ref droplet, -50, 10, gridstep, ref grassspr, new Rectangle(0, 0, 60, 60), new Vector2(1440, 900));
                Components.Add(grass);
                mushroom = new StaticPrey(this, ref droplet, -50, -20, gridstep, ref mushroomspr, new Rectangle(0, 0, 60, 60), new Vector2(1800, 960));
                Components.Add(mushroom);

            }
            if (state == State.level2)
            {
              
                mushroom = new StaticPrey(this, ref droplet, -50, -20, gridstep, ref mushroomspr, new Rectangle(0, 0, 60, 60), new Vector2(1800, 900));
                Components.Add(mushroom);
                centipede = new DynamicPrey(this, ref droplet, -100, -100, gridstep, ref centipedespr, ref centipedePRspr, ref centipedePLspr, new Rectangle(0, 0, 60, 60), new Vector2(1680, 840));
                Components.Add(centipede);
                centipede2 = new DynamicPrey(this, ref droplet, -100, -100, gridstep, ref centipedespr, ref centipedePRspr, ref centipedePLspr, new Rectangle(0, 0, 60, 60), new Vector2(240, 180));
                Components.Add(centipede2);
                bluegrass = new StaticPrey(this, ref droplet, -50, -20, gridstep, ref bluegrassspr, new Rectangle(0, 0, 60, 60), new Vector2(1740, 960));
                Components.Add(bluegrass);
                polypore = new StaticPrey(this, ref droplet, -50, -20, gridstep, ref polyporespr, new Rectangle(0, 0, 60, 60), new Vector2(60, 300));
                Components.Add(polypore);
                peas = new StaticPrey(this, ref droplet, -50, 20, gridstep, ref peasspr, new Rectangle(0, 0, 60, 60), new Vector2(1140, 780));
                Components.Add(peas);
            }
        }

        protected void CreateNewObject()
        {
            droplet = new Droplet(this, ref sprscale, ref dr, new Rectangle(0,0,60,60), new Vector2(300, 960), gridstep);
          
            Components.Add(droplet);
        }        void ChangeFrame(float elpT)
        {
            totalElapsed += elpT;
            if (totalElapsed > timePerFrame)
            {
                if (numberOfFrame == frames - 1)
                {
                    f = false;
                    //numberOfFrame = 0;
                }
                else numberOfFrame++;
                gameoverrec = new Rectangle((int)widthFrame * numberOfFrame, 0, widthFrame, heightFrame);
                totalElapsed = 0;
            }

        }
        protected override void Update(GameTime gameTime)
        {
            if (state == State.gameover && f == true)
            {
                ChangeFrame((float)gameTime.ElapsedGameTime.TotalSeconds);
                
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            //if (Keyboard.GetState().IsKeyDown(Keys.M)) SetState(1);
            //if (Keyboard.GetState().IsKeyDown(Keys.N)) SetState(3);
            //if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            //{
            //    result = new PlayerResult(nameScreen.Name);
            //    SetState(2);
            //}



            if (nameScreen.Enabled == true && Keyboard.GetState().IsKeyDown(Keys.Enter) && nameScreen.nameReady)//и если имя валидно!
            {
                result = new PlayerResult(nameScreen.Name);
                nameScreen.ClearName();
                SetState(2);
            }
            //условие поменять!
            if ((state == State.gameover || state == State.win ) && Keyboard.GetState().IsKeyDown(Keys.Enter)) SetState(1); //выход в меню по кнопке ентер после победы или поражения

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.AntiqueWhite);


            spriteBatch.Begin();

            if (state == State.gameover)
            {
                spriteBatch.Draw(gameoverspr, gameoverpos, gameoverrec, Color.White);
                base.Draw(gameTime);
            }
            if (state == State.menu)
            {
                spriteBatch.Draw(fm, Vector2.Zero, Color.White);
                //spriteBatch.Draw(menuspr, Vector2.Zero, Color.White);
                base.Draw(gameTime);
            }
            if (state == State.level1)
            {
                spriteBatch.Draw(f1, Vector2.Zero, Color.White);
                spriteBatch.Draw(sprscales, Vector2.Zero, Color.White);

                base.Draw(gameTime);
                spriteBatch.Draw(decor2, new Vector2(0, 620), Color.White);


            }
            if (state == State.level2)
            {
                spriteBatch.Draw(f2, Vector2.Zero, Color.White);
                spriteBatch.Draw(sprscales, Vector2.Zero, Color.White);

                base.Draw(gameTime);
            }
            if (state == State.win) //если победа
            {
                //spriteBatch.Draw(f2, Vector2.Zero, Color.White);
                spriteBatch.Draw(winspr, new Vector2(410,430), Color.White);


                base.Draw(gameTime);
            }
            //ShowGrid();
            spriteBatch.End();
        }

        private void ShowGrid()
        {
            for (int i = 0; i < 1080; i += gridstep)
            {
                for (int j = 0; j < 1920; j += gridstep)
                {
                    spriteBatch.Draw(grid, new Vector2(j, i), Color.White);
                }
            }
        }

        public Coord  WhatIsIt(Vector2 place)//для несеточных координат
        { 
          
            int j = (int)(place.X / gridstep);
            int i = (int)(place.Y / gridstep);

            if ((i < 0) || (j < 0) || (i > 17) || (j > 31)) return new Coord(-1, -1, -1);

             switch (level[i, j])
            {

                case 1: return new Coord(1, i, j);
                case 2: return new Coord(2, i, j);
                case 3: return new Coord(3, i, j);
                default: return new Coord(0, i, j);
            }
            
        }
        public Coord WhatIsIt(int i, int j) // для сеточных координат
        {
      
            if ((i < 0) || (j < 0) || (i > 17) || (j > 31)) return new Coord(-1, -1, -1);

            switch (level[i, j])
            {

                case 1: return new Coord(1, i, j);
                case 2: return new Coord(2, i, j);
                case 3: return new Coord(3, i, j);
                default: return new Coord(0, i, j);
            }
            //return 0;
        }
    }
}
