﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;

namespace droplet_devours
{
    class StaticPrey : Prey
    {
        public StaticPrey(World game, ref Droplet droplet, int minddamage, int healthdamage, int gridstep, ref Texture2D texture, Rectangle rectangle, Vector2 position) : base(game, ref droplet, minddamage, healthdamage, gridstep, ref texture, rectangle, position)
        {

        }
        public override void Initialize()
        {
            base.Initialize();
        }

        }
    }
